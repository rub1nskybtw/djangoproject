from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.conf import settings
from .gistology import *
import os
import shutil
def histology(request):
    path = os.path.join(os.path.abspath(os.path.dirname(__file__)) + '/media/', 'test_img')
    shutil.rmtree(path)
    if not os.path.isdir(THIS_FOLDER + '/media/test_img'):
        os.makedirs(THIS_FOLDER + '/media/test_img')
    path = os.path.join(os.path.abspath(os.path.dirname(__file__)) + '/media/', 'test_gt')
    shutil.rmtree(path)
    if not os.path.isdir(THIS_FOLDER + '/media/test_gt'):
        os.makedirs(THIS_FOLDER + '/media/test_gt')
    path = os.path.join(os.path.abspath(os.path.dirname(__file__)) + '/media/', 'segmentation')
    shutil.rmtree(path)
    if not os.path.isdir(THIS_FOLDER + '/media/segmentation'):
        os.makedirs(THIS_FOLDER + '/media/segmentation')


    if request.method == 'POST' and request.FILES['myfile']:
       myfile = request.FILES['myfile']
       if str(myfile) != [] and str(myfile) != [''] and str(myfile) != None:
           if os.path.isfile(THIS_FOLDER + "/media/test_img/" + str(myfile)) == True:
               os.remove(THIS_FOLDER + "/media/test_img/" + str(myfile))
           if os.path.isfile(THIS_FOLDER + "/media/test_gt/" + str(myfile)) == True:
               os.remove(THIS_FOLDER + "/media/test_gt/" + str(myfile))
           if os.path.isfile(THIS_FOLDER + "/media/segmentation/" + str(myfile)) == True:
               os.remove(THIS_FOLDER + "/media/segmentation/" + str(myfile))


       fs = FileSystemStorage(location=settings.MEDIA_ROOT +'/test_img')
       filename = fs.save(myfile.name, myfile)
       uploaded_file_url = fs.url(filename)
       if os.path.isfile(THIS_FOLDER + "/media/test_img/" + str(myfile)) == True:
        predictor(str(myfile))
        image_markup(str(myfile))
       return render(request, 'mainapp/histology.html', {'uploaded_file_url': uploaded_file_url,"fileupload":"File uploaded successfully"})
    return render(request, 'mainapp/histology.html')

def index(request):
    return render(request, 'mainapp/index.html')

def map(request):
    return render(request, "mainapp/map.html")

def pulls(request):
    return render(request, "mainapp/pulls.html")