from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('histology',views.histology, name='histology'),
    path('map', views.map, name='map'),
    path('pulls', views.pulls, name='pulls')
]
